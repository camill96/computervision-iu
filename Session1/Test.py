import numpy as np
import time
import cv2

if __name__ == "__main__":

	cap = cv2.VideoCapture(0)
	while (True):
		start = time.time()
		#Capture frame by frame
		ret, frame = cap.read()

		#Our operations on the frame come here
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		
		#Display the resulting frame
		cv2.imshow('frame', gray)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
		time.sleep(0.1)
		#print (1/(time.time() - start))

	#When everything done, release the capture
	cap.release()
	cv2.destroyAllWindows