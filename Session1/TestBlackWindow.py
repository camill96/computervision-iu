import numpy as np
import time
import cv2

if __name__ == "__main__":

	cap = cv2.VideoCapture(0)

	while (True):

		#Capture frame by frame
		ret, frame = cap.read()

		#Create a black image
		img = np.zeros((512, 512, 3), np.uint8)

		#Draw a diagonal blue line with thickness of 5px
		cv2.line(img, (0, 0), (511, 511), (255, 0, 0), 5)

		

		#Our operations on the frame come here
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		
		#Display the resulting frame
		cv2.imshow('frame', img)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	print gray
	print cap
	#When everything done, release the capture
	cap.release()
	cv2.destroyAllWindows